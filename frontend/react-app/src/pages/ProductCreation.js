import {useState, useEffect, useContext} from 'react';

import { Form, Button, Icon, Dropdown  } from 'react-bootstrap';

import UserContext from '../UserContext';

import {useNavigate, Navigate} from 'react-router-dom'

import Swal from 'sweetalert2';


export default function CreateProduct() {

	const navigate = useNavigate();

	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [stock, setStock] = useState("");
	const [player, setPlayer] = useState([]);
	const [chosenPlayer, setChosenPlayer] = useState("");
	const [isActive, setIsActive] = useState(false);
	const [isValid, setIsValid] = useState(false);
	const {user} = useContext(UserContext);

	const handlePlayerSelect = (e) => {
		setChosenPlayer(e.target.textContent);
	  };


	function addProduct(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: productName,
				description: description,
				price: price,
				stock: stock,
				isActive: isActive,
				player: chosenPlayer
			})
		})
		.then(res => res.json())
		.then(data => {
		console.log(data)

		if(data === true) {
			Swal.fire({
				title: "Successfully created product!",
				icon: "success",
				text: `You have successfully added ${productName}.`
			})

			navigate("/login")

		} else if (data === false) {
			Swal.fire({
				title: "Something went wrong",
				icon: "error",
				text: "Please try again."
			})
		} 
	});

};



	useEffect(() => {
		if (
			productName !== "" &&
			description !==""&&
			price !==""&&
			stock !== "" && 
			player !== "" && 
			isActive !== "" 
			)
			{setIsValid(true);
		} else {
			setIsValid(false)
		}
	}, [productName, description, stock]);

	useEffect(() => {
	  fetch(`${process.env.REACT_APP_API_URL}/players/all`)
		.then(res => res.json())
		.then(data => setPlayer(data));
	}, []);

  
	return (
		(user.isAdmin === true || user.isSeller === true) ?
		<Form onSubmit={e => addProduct(e)} className="mt-5 mx-auto half-width-form">

			<Form.Group controlId="productName" className="turn-white mt-4">
				<Form.Label>Product Name</Form.Label>
				<Form.Control 
					type="productName" 
					placeholder="Enter Product Name" 
					value={productName}
					onChange={(e) => setProductName(e.target.value)}
					required

				/>
			</Form.Group>

			 <Form.Group controlId="description" className="turn-white mt-4">
				<Form.Label>Product Description</Form.Label>
				<Form.Control 
					type="description" 
					placeholder="Enter product description" 
					value={description}
					onChange={(e) => setDescription(e.target.value)}
					required

				/>
			</Form.Group>

			<Form.Group controlId="player" className="turn-white mt-4">
					<Form.Label>Player who uses this item</Form.Label>
					<Dropdown>
					  <Dropdown.Toggle variant="secondary" id="dropdown-basic">
						{chosenPlayer}
					  </Dropdown.Toggle>

					  <Dropdown.Menu>
						{player.map(p => (
						  <Dropdown.Item key={p._id} onClick={handlePlayerSelect}>{p.name}</Dropdown.Item>
						))}
					  </Dropdown.Menu>
					</Dropdown>
			</Form.Group>


			<Form.Group controlId="price" className="turn-white mt-4">
				<Form.Label>Product Price</Form.Label>
				<Form.Control 
					type="price" 
					placeholder="Enter product price (in Pesos)" 
					value={price}
					onChange={(e) => setPrice(e.target.value)}
					required

				/>
			</Form.Group>


			<Form.Group controlId="stock" className="turn-white mt-4">
				<Form.Label>Stock</Form.Label>
				<Form.Control 
					type="stock" 
					placeholder="Enter product stocks" 
					value={stock}
					onChange={(e) => setStock(e.target.value)}
					required

				/>
			</Form.Group>

			<Form.Group controlId="isActive" className="turn-white my-4">
			  <Form.Label>Product Visibility</Form.Label>
			  <div style={{ display: "flex", alignItems: "center" }}>
				<Button
				  variant={isActive ? "success" : "danger"}
				  onClick={() => setIsActive(!isActive)}
				>
				  {isActive ? "Visible" : "Hidden"}
				</Button>
				<Form.Text className="text-muted" style={{ paddingLeft: '10px' }}>
				  Will the product be visible right after creation?
				</Form.Text>
			  </div>
			</Form.Group>



			{isValid ?
			<div className="d-flex justify-content-center">
				<Button className="my-4" variant="success" type="submit" id="submitBtn" size="lg">
					Create Product
				</Button>
			</div>

			:
			<div className="d-flex justify-content-center">
				<Button className="my-4" variant="danger" type="submit" id="submitBtn" size="lg" disabled>
					Create Product
				</Button>
			</div>
			}

			
		</Form>
		:
		<Navigate to="/noaccess"/>
	
	)

}


