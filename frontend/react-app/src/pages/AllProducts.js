import ProductCard from '../components/ProductCard';
import {useState, useEffect} from 'react';
import { Container, Row, Col } from 'react-bootstrap';

export default function Products(){


	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} product={product} />
					)
			}))
		})
	}, [])


	return (
		<Container className="mt-3">
		  <Row xs={1} sm={2} md={3} className="g-4">
			{products.map((product) => (
			  <Col key={product._id}>
				<>{product}</>
			  </Col>
			))}
		  </Row>
		</Container>
	  );
	}