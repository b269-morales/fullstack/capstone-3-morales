import {useState, useEffect, useContext} from 'react';

import { Form, Button, Icon, Dropdown  } from 'react-bootstrap';

import UserContext from '../UserContext';

import {useNavigate, Navigate} from 'react-router-dom'

import Swal from 'sweetalert2';


export default function AddPlayer() {

	const navigate = useNavigate();

   
	const [player, setPlayer] = useState('');
	const [isValid, setIsValid] = useState(false);
	const {user} = useContext(UserContext);



	function addPlayer(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/players/add`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: player
			})
		})
		.then(res => res.json())
		.then(data => {
		console.log(data)

		if(data === true) {
			Swal.fire({
				title: "Successfully added player!",
				icon: "success",
				text: `You have successfully added ${player}.`
			})

			navigate("/login")

		} else if (data === false) {
			Swal.fire({
				title: "Something went wrong",
				icon: "error",
				text: "Please try again."
			})
		} 
	});

};



	useEffect(() => {
		if (
			player !== ""
			)
			{setIsValid(true);
		} else {
			setIsValid(false)
		}
	}, [player]);


  
	return (
		(user.isAdmin === true ) ?
		<Form onSubmit={e => addPlayer(e)} className="mt-5 mx-auto half-width-form">

			<Form.Group controlId="player" className="turn-white">
				<Form.Label >Player's Name</Form.Label>
				<Form.Control 
					type="playerName" 
					placeholder="Enter Player's Name" 
					value={player}
					onChange={(e) => setPlayer(e.target.value)}
					required

				/>
			</Form.Group>


			{isValid ?
			<div className="d-flex justify-content-center">
				<Button className="my-4" variant="primary" type="submit" id="submitBtn" size="lg">
					Add Player
				</Button>
			</div>

			:
			<div className="d-flex justify-content-center">
				<Button className="my-4" variant="danger" type="submit" id="submitBtn" size="lg" disabled>
					Add Player
				</Button>
			</div>
			}

			
		</Form>
		:
		<Navigate to="/noaccess"/>
	
	)

}


