import { useState, useEffect } from 'react';
import { Accordion, Card, Button } from 'react-bootstrap';

export default function Orders() {
	const [orders, setOrders] = useState([]);
	const [orderGrandTotal, setOrderGrandTotal] = useState(0);

	useEffect(() => {
		const fetchOrders = async () => {
			try {
				const response = await fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
					headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`
					}
				});
				const data = await response.json();
				console.log(data.orders)
				setOrders(data.orders);
				setOrderGrandTotal(data.orderGrandTotal);
			} catch (error) {
				console.error(error);
			}
		};
		fetchOrders();
	}, []);

	const orderItems = [];
	for (let i = 0; i < orders.length; i++) {
		const order = orders[i];
		orderItems.push(
			<Card key={order.orderId}>
				<Card.Header>
					<Accordion.Toggle as={Button} variant="link" eventKey={order.orderId}>
						Order ID: {order.orderId} - {order.productName} ({order.quantity})
					</Accordion.Toggle>
				</Card.Header>
				<Accordion.Collapse eventKey={order.orderId}>
					<Card.Body>
						<p>Status: {order.status}</p>
						<p>Ordered On: {order.orderedOn}</p>
						<p>Subtotal: {order.subtotal}</p>
					</Card.Body>
				</Accordion.Collapse>
			</Card>
		);
	}

	return (
		<Accordion>
			{orderItems}
			<Card>
				<Card.Header>
					<Accordion.Toggle as={Button} variant="link" eventKey="grandTotal">
						Grand Total: {orderGrandTotal}
					</Accordion.Toggle>
				</Card.Header>
			</Card>
		</Accordion>
	);
};
