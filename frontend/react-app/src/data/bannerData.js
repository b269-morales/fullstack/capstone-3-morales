const bannerData = [
    {
        id: "bannerHomeNew",
        name: "Going pro.",
        description: "Get the peripherals your favorite e-sport pros use",
        button: "Try Now!",
        dest: "register"
    },
    {
        id: "bannerHomeReturn",
        name: "Going pro.",
        description: "Get the peripherals your favorite e-sport pros use",
        button: "Browse Products!",
        dest: "products"
    },
    {
        id: "bannerError",
        name: "Error 404 - Page not found.",
        description: "The page you are looking for cannot be found.",
        button: "Back to home.",
        dest: ""
    },
    {
        id: "bannerNoAccess",
        name: "User does not have access.",
        description: "You do not have access to this page.",
        button: "Back to home.",
        dest: ""
    }
]

export default bannerData;