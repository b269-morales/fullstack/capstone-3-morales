
import {Container, Spinner} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import React from 'react';



import {UserProvider} from './UserContext';
import Home from './pages/Home';
import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';
import ProductEdit from './pages/ProductEdit';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import AllProducts from './pages/AllProducts';
import ProductCreation from './pages/ProductCreation';
import AddPlayer from './pages/AddPlayer';
import Error from './pages/Error';
import NoAccess from './pages/NoAccess';


import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'

import './App.css';


function App() {

 
	const [isLoading, setIsLoading] = useState(true);
	const [user, setUser] = useState({
			id: null,
			isAdmin: false,
			isSeller: false,
			orders: null,
			cart: null,
			name: null
	});

	const unsetUser = () => {
		localStorage.clear();
	};


	useEffect(() => {
		const token = localStorage.getItem('token');
		if (token) {
			fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
				headers: {
					Authorization: `Bearer ${token}`
				},
			})
			.then(res => res.json())
			.then(data => {
				console.log("API response data: ", data);
				// User is logged in
				if (typeof data._id !== "undefined"){
					setUser({
						id: data._id,
						isAdmin: data.isAdmin,
						isSeller:data.isSeller,
						cart: data.cart,
						orders: data.orders,
						name: data.firstName
					})
				} 
				// User is logged out
				else {
					setUser({
						id: null,
						isAdmin: null,
						isSeller: null,
						cart: null,
						orders: null,
						name: null
					})
				}
				setIsLoading(false);
			})
			.catch(error => {
				console.error('Error fetching user details:', error);
				// If there's an error, log the user out
				setUser({
					id: null,
					isAdmin: null,
					isSeller: null,
					cart: null,
					orders: null,
					name: null
				});
				localStorage.clear();
				setIsLoading(false);
			})
		} else {
			setUser({
				id: null,
				isAdmin: null,
				isSeller: null,
				cart: null,
				orders: null,
				name: null
			});
			setIsLoading(false);
		}
	}, []);


	return (
		// '<> </>' fragments - common pattern in React for component to return multiple elements
		<>
		<UserProvider value={{user, setUser, unsetUser}}>
			<Router>
					<AppNavbar />
				<Container>
				{isLoading ? (
						<div className="d-flex justify-content-center align-items-center" style={{ height: '100vh' }}>
							<Spinner animation="border" role="status">
								<span className="visually-hidden">Loading...</span>
							</Spinner>
						</div>
						) : (
					<Routes>
						<Route path="/" element={<Home/>} /> 
						<Route path="/register" element={<Register/>} /> 
						<Route path="/login" element={<Login/>} />
						<Route path="/addplayer" element={<AddPlayer/>} />
						<Route path="/addproduct" element={<ProductCreation/>} />
						<Route path="/products" element={<Products/>} /> 
						<Route path="/products/:productId" element={<ProductView/>} /> 
						<Route path="/products/:productId/edit" element={<ProductEdit/>} /> 
						<Route path="/allproducts" element={<AllProducts/>} /> 
						<Route path="/logout" element={<Logout/>} />
						<Route path="/noaccess" element={<NoAccess/>} />
						<Route path="/*" element={<Error/>} />
					</Routes>
					)}
				</Container>
			</Router>
		</UserProvider>
		</>
	)
};

export default App;


