import { useState, useContext, useEffect } from 'react';
import {Button, Offcanvas} from 'react-bootstrap';
import { FiTruck } from 'react-icons/fi';
import { FaTrashAlt } from 'react-icons/fa';
import Swal from 'sweetalert2';

import UserContext from '../UserContext'

export default function UserOrders() {
	const [show, setShow] = useState(false);

	const [update, setUpdate] = useState(false);


	const [cart, setCart] = useState([]);
	const [cartGrandTotal, setCartGrandTotal] = useState(0);

	const handleClose = () => setShow(false);
	const handleShow = () => {
		setShow(true);
		setUpdate(!update);
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		})
		.then((res) => res.json())
		.then((data) => {
			console.log(data);
			setCart(data.orders);
			setCartGrandTotal(data.orderGrandTotal);
		})
		.catch((error) => console.log(error));
	}, [update]);



	return (
		<>
		<Button variant="link" onClick={handleShow} className="me-2 icon-buttons" style={{color: 'white'}}>
		<FiTruck style={{color: 'white'}}/>
		</Button>
		<Offcanvas show={show} onHide={handleClose} placement="end">
		<Offcanvas.Header closeButton>
		<Offcanvas.Title>Orders</Offcanvas.Title>
		</Offcanvas.Header>
		<Offcanvas.Body>
		{cart && cart.length > 0 ? (
			<div>
			{cart.map((item) => (
				<div key={item.orderId}>
				<p>{item.productName}</p>
				<p>Quantity: {item.quantity}</p>
				<p>Subtotal: {item.subtotal}</p>
				<p>Order Date: {item.orderedOn}</p>
				<p>Status: {item.status}</p>
				<hr />
				</div>
				))}

			</div>
			) : (
			<p>Your orders is empty</p>
			)}
			</Offcanvas.Body>
			</Offcanvas>
			</>
			);



}

