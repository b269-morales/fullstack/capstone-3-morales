import {useState, useEffect, useContext} from 'react';

import {Link} from 'react-router-dom';

import { Button, Row, Col, Card } from 'react-bootstrap';

import { FaRegEdit } from 'react-icons/fa';

import UserContext from '../UserContext';

import Swal from 'sweetalert2'



export default function ProductCard({product}) {

	const { productName, productDescription, productPrice, stock, _id } = product;

	const { user } = useContext(UserContext);

const addToCart = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/addToCart`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: 1
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data !== null) {
				Swal.fire({
					title: "Successfully added to cart!",
					icon: "success",
					text: `You have successfully added ${productName} to your cart.`
				})

			} else {
				Swal.fire({
					title: "Something went wrong.",
					icon: "error",
					text: "Please try again."
				})
			}

		})
	};


return (
	<Row className="mt-3 mb-3">
		<Col xs={12}>
			<Card className="cardHighlight ">
				
					<Card.Body className="px-5">
						<Card.Title className="text-center"><h4>{productName}</h4></Card.Title>
						<hr/>
						<Card.Subtitle>Description</Card.Subtitle>
						<Card.Text>{productDescription}</Card.Text>
						<Card.Subtitle>Price</Card.Subtitle>
						<Card.Text>{productPrice}</Card.Text>
						<Card.Subtitle>Stock</Card.Subtitle>
						<Card.Text>{stock}</Card.Text>

						{user.id === null  && (
							<div className="d-flex justify-content-center">
								<Button variant="danger" className="pt-2 px-5 py-2"   as={Link} to={`/products/${_id}`}>Details</Button>
							</div>
						)}

						{user.id !== null && !user.isAdmin && !user.isSeller &&
							(<>
							<div className="d-flex justify-content-center">
								<Button variant="danger" className="mx-3 px-4"  as={Link} to={`/products/${_id}`}>Details</Button>
								<Button variant="danger" className="mx-3" onClick={(event) => { event.stopPropagation(); addToCart(_id) }}>Add To Cart</Button>
							</div>
							</>
							)

						}

						{user && (user.isAdmin || user.isSeller) && (
						<div className="d-flex justify-content-center">
							<Button variant="danger" className=" pt-2 px-5 py-3" as={Link} to={`/products/${_id}/edit`}> 
								<div className="icon-buttons">
									<FaRegEdit /> 
								</div>

							</Button>
						</div>
						)}
					</Card.Body>
				
			</Card>
		</Col>
	</Row>        
	)


}

