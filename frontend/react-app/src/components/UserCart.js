import { useState, useContext, useEffect } from 'react';
import {Button, Offcanvas} from 'react-bootstrap';
import { SlBasket } from 'react-icons/sl';
import { FaTrashAlt } from 'react-icons/fa';
import Swal from 'sweetalert2';

import UserContext from '../UserContext'

 export default function UserCart() {
	const [show, setShow] = useState(false);

	const [update, setUpdate] = useState(false);

	const [isSelected, setIsSelected] = useState(false);

	const [cart, setCart] = useState([]);
	const [cartGrandTotal, setCartGrandTotal] = useState(0);

	const handleClose = () => setShow(false);
	const handleShow = () => {
		setShow(true);
		setUpdate(!update);
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/cart`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				console.log(data);
				setCart(data.cart);
				setCartGrandTotal(data.cartGrandTotal);
			})
			.catch((error) => console.log(error));
	}, [update]);


		const removeFromCart = (cartItemId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/cart/delete`, {
			method: "DELETE",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
			body: JSON.stringify({
				cartItemId: cartItemId
			})
		})
			.then((res) => res.json())
			.then((data) => {
				console.log(data);
				setCart(data.cart);
				setCartGrandTotal(data.cartGrandTotal);
			})
			.catch((error) => console.log(error));
	};


		const editCart = (productId, quantity) => {
				fetch(`${process.env.REACT_APP_API_URL}/users/cart/edit`, {
					method: "PATCH",
					headers: {
						"Content-Type": "application/json",
						Authorization: `Bearer ${localStorage.getItem("token")}`,
					},
					body: JSON.stringify({
						productId: productId,
						quantity: quantity
					})
				})
					.then((res) => res.json())
					.then((data) => {
						console.log(data);
						setCart(data.cart);
						setCartGrandTotal(data.cartGrandTotal);
					})
					.catch((error) => console.log(error));
			};

			const checkout = () => {
				fetch(`${process.env.REACT_APP_API_URL}/users/cart/order`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						Authorization: `Bearer ${localStorage.getItem("token")}`,
					}
				})
					.then((res) => res.json())
					.then((data) => {

						console.log(data)

						if(data !== false) {
							handleClose()
							Swal.fire({
								title: "Successfully ordered!",
								icon: "success",
								text: `You have successfully ordered the items in your cart.`
							})

							/*navigate(`/products/${productId}`)*/ //insert orders here

						} else {
							Swal.fire({
								title: "Something went wrong.",
								icon: "error",
								text: "Please try again."
							})
						}

					})
					.catch((error) => console.log(error));
					setUpdate(!update)
			};







	return (
		<>
			<Button variant="link" onClick={handleShow} className="me-2 icon-buttons">
				<SlBasket style={{color: 'white'}}/>
			</Button>
			<Offcanvas show={show} onHide={handleClose} placement="end">
				<Offcanvas.Header closeButton>
					<Offcanvas.Title>Shopping Cart</Offcanvas.Title>
					<hr/>
				</Offcanvas.Header>
				<Offcanvas.Body>
					{cart.length > 0 ? (
						<div>
							{cart.map((item) => (
								<div key={item.productId}>
									<p className="offcanvas-product-name">{item.productName}</p>
									<p>Quantity: {item.quantity}</p>
									<p>Subtotal: {item.subtotal}</p>
									<Button variant="danger" className="icon-buttons" onClick={() => removeFromCart(item._id)}>
										<FaTrashAlt style={{ color: 'white' }} />
									</Button>
									<hr />
								</div>
							))}
							<p className="cart-total">Cart Total: {cartGrandTotal}</p> 
							<Button variant="success" className="float-right" onClick={checkout}>Checkout</Button>

						</div>
					) : (
						<p>Your cart is empty</p>
					)}
				</Offcanvas.Body>
			</Offcanvas>
		</>
	);

	}

