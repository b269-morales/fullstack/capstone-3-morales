import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext';


import { Container, Card, Button, ButtonGroup, Row, Col, Form, OverlayTrigger, Popover } from 'react-bootstrap';

import Swal from 'sweetalert2';

import {useParams, useNavigate, Link} from 'react-router-dom';

export default function ProductView() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();
	// "useParams" hook that will allow us to retrieve the courseId passed via URL params
	// http://localhost:3000/courses/642b9ee764ba1150b3a1a4e1
	const {productId} = useParams();
	
	const [productName, setProductName] = useState("");
	const [quantity, setQuantity] = useState(1);
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [stock, setStock] = useState("");
	const [isActive, setIsActive] = useState(false);

	const increaseQuantity = () => {
		const newQuantity = parseInt(quantity) + 1;
		setQuantity(newQuantity <= stock ? newQuantity : quantity);
	}

	const decreaseQuantity = () => {
		const newQuantity = parseInt(quantity) - 1;
		setQuantity(newQuantity > 0 ? newQuantity : 1);
	}

	const customQuantity = (e) => {
		const value = parseInt(e.target.value) || 1;
		setQuantity(Math.min(value, stock));
	};


	const addToCart = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/addToCart`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data !== null) {
				Swal.fire({
					title: "Successfully added to cart!",
					icon: "success",
					text: `You have successfully added ${productName} to your cart.`
				})

				navigate(`/products/${productId}`)

			} else {
				Swal.fire({
					title: "Something went wrong.",
					icon: "error",
					text: "Please try again."
				})
			}

		})
	};

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProductName(data.productName);
			setDescription(data.productDescription);
			setPrice(data.productPrice);
			setStock(data.stock);
		})
	}, [productId])

	return (

		<Container>
		<Row>
		<Col lg={{span: 6, offset:3}} >
		<Card className="cardView mt-5">
		<Card.Body className="text-center">
		<Card.Title className="pt-5">{productName}</Card.Title>
		<hr/>
		<Card.Subtitle className="pt-5">Description:</Card.Subtitle>
		<Card.Text>{description}</Card.Text>
		<Card.Subtitle>Price:</Card.Subtitle>
		<Card.Text>PhP {price}</Card.Text>
		<Card.Subtitle>Stock</Card.Subtitle>
		<Card.Text>{stock}</Card.Text>
		{
			(user.id !== null) ?
				(user.isAdmin === false && user.isSeller === false) ?
			<Button variant="danger" className="mt-5 px-5 py-3" onClick={() => addToCart(productId)} >Add To Cart</Button>
			:
			<Button variant="danger" className="mt-5 px-5 py-3" disabled>Not Accessible</Button>
			:
			<Button className="btn btn-danger mt-5" as={Link} to="/login" size="lg"  >Log in to Purchase</Button>
		}

		<div>
		{ user.id !== null && (<ButtonGroup className="mt-3" >
						<Button variant="secondary" onClick={decreaseQuantity}>-</Button>
						<OverlayTrigger
						trigger="click"
						key='bottom'
						placement='bottom'
						overlay={
							<Popover id={`popover-positioned-bottom`}>
							<Popover.Header as="h3">{`Quantity`}</Popover.Header>
							<Popover.Body>
							<Form.Control type="number" value={quantity} max={stock} min={1} onChange={customQuantity} style={{width: '4rem'}} />
							</Popover.Body>
							</Popover>
						}
						>
						<Button variant="secondary">{quantity}</Button>
						</OverlayTrigger>
						<Button variant="secondary" onClick={increaseQuantity}>+</Button>
						</ButtonGroup>)}
		</div>


		</Card.Body>
		</Card>
		</Col>
		</Row>
		</Container>

		)
}