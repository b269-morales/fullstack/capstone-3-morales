const express = require("express");

// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

const playerController = require("../controllers/playerController.js");


const auth = require("../auth");



router.get("/all", (req,res) => {
	playerController.getAllPlayers().then(
		resultFromController => res.send(resultFromController)
		);
});




router.post("/add", auth.verify, (req, res) => {

	const data = {
		name: req.body.name,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		isSeller: auth.decode(req.headers.authorization).isSeller,
	}
	playerController.addPlayer(data).then(resultFromController => res.send(resultFromController));
});




/*=============================*/
	module.exports = router;
/*=============================*/