const express = require("express");

// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

const userController = require("../controllers/userController.js");


const auth = require("../auth");

router.get("/all", (req,res) => {
	userController.getAllUser().then(
		resultFromController => res.send(resultFromController)
		);
});

router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(
		resultFromController => res.send(resultFromController)
		);
});

router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(
		resultFromController => res.send(resultFromController)
		);
});



router.patch("/access", auth.verify, (req, res) => {
	const data = {
		user: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.updateUserAccess(data, req.body).then(
		resultFromController => res.send(resultFromController)
		);
});

router.get("/admin/details", auth.verify, (req,res) => {

	const data = {
		user: req.body.user,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.getProfile(data).then(
		resultFromController => res.send(resultFromController)
		);
});

router.get("/details", auth.verify, (req,res) => {

	const data = {
		user: auth.decode(req.headers.authorization).id
	}

	userController.getDetails(data).then(
		resultFromController => res.send(resultFromController)
		);
});


router.get("/cart", auth.verify, (req,res) => {

	const data = auth.decode(req.headers.authorization).id;

	userController.getCart(data).then(
		resultFromController => res.send(resultFromController)
		);
});

router.get("/orders", auth.verify, (req,res) => {

	const data = auth.decode(req.headers.authorization).id;

	userController.getOrders(data).then(
		resultFromController => res.send(resultFromController)
		);
});


router.patch("/cart/edit", auth.verify, (req, res) => {
	const data = {
		user: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
	}
	userController.updateCartItemQuantity(data).then(
		resultFromController => res.send(resultFromController)
		);
});



router.delete("/cart/delete", auth.verify, (req, res) => {
	userController.removeFromCart(auth.decode(req.headers.authorization).id, req.body.cartItemId)
	.then(resultFromController => res.send(resultFromController));
});




router.post("/cart/order", auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	const data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	userController.addToOrders(data).then(resultFromController => 
		res.send(resultFromController));
});






/*=============================*/
module.exports = router;
/*=============================*/