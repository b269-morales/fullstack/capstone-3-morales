const mongoose = require("mongoose");

const playerSchema = new mongoose.Schema({
	
			name: {
				type: String,
				required: [true, "Player is required"],
			}
		
});


module.exports = mongoose.model("Player", playerSchema);