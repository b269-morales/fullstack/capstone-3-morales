const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	productName : {
		type : String,
		required : [true, "Name is required"]
	},
	productDescription : {
		type : String,
		required : [true, "Description is required"]
	},
	productPrice : {
		type : Number,
		required : [true, "Price is required"]
	},
	isActive: {
		type : Boolean,
	default : true
	},
	stock: {
		type: Number,
		required : [true, "Stock is required"]
	},
	pro: [
		{
			player: {
				type: String,
				required: [true, "Player is required"],
			}
		},
		]
});


module.exports = mongoose.model("Product", productSchema);