const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	userName: {
		type: String,
		required: [true, "User name is required"],
	},
	firstName: {
		type: String,
		required: [true, "First name is required"],
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"],
	},
	email: {
		type: String,
		required: [true, "Email is required"],
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile No is required"],
	},
	password: {
		type: String,
		required: [true, "Password is required"],
	},
	isAdmin: {
		type: Boolean,
	default: false,
	},
	isSeller: {
		type: Boolean,
	default: false,
	},
	cart: [
	{
		productId: {
			type: String,
			required: [true, "Product ID is required"],
		},
		productName: {
			type: String,
			required: [true, "Product Name is required"],
		},
		productPrice: {
			type: Number,
			required: [true, "Price is required"],
		},
		quantity: {
			type: Number,
			required: [true, "Quantity is required"],
		default: 1,
		},
		subtotal: {
			type: Number,
			required: [true, "Subtotal is required"],
		},
		isSelected: {
			type: Boolean,
			required: [true, "Subtotal is required"],
			default: false,
		},
	},
	],
	orders: [
	{
		orderId: {
			type: String,
			required: [true, "Order ID is required"],
		},
		productName: {
			type: String,
			required: [true, "Product is required"],
		},
		quantity: {
			type: Number,
			required: [true, "Quantity is required"],
		default: 1,
		},
		subtotal: {
			type: Number,
			required: [true, "Subtotal is required"],
		},
		orderedOn: {
			type: Date,
		default: new Date(),
		},
		status: {
			type: String,
		default: "Ordered",
		},
	},
	],
	cartGrandTotal: {
		type: Number,
	default: 0,
	},
	orderGrandTotal: {
		type: Number,
	default: 0,
	},
});

module.exports = mongoose.model("User", userSchema);
