const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId : {
		type : mongoose.Schema.Types.ObjectId,
		ref: "User",
		required : [true, "User ID is required"]
	},
	products : [{
		productId : {
			type: mongoose.Schema.Types.ObjectId,
			required : [true, "Product ID is required"]
		},
		quantity : {
			type: Number,
			required : [true, "quantity is required"]
		},
		subtotal : {
			type: Number,
			required : [true, "Subtotal is required"]
		},
	}],
	totalAmount : {
		type : Number,
		required : [true, "Price is required"]
	},
	orderedOn : {
		type : Date,
	default : new Date() 
	},
	status: {
		type : String,
	default : "Order Placed"
	}
});


module.exports = mongoose.model("Order", orderSchema);